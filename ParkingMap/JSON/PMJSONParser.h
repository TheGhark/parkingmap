//
//  PMJSONParser.h
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMLocation.h"

typedef void (^ _Nonnull ParserHandler)(PMLocation * _Nullable location, NSError  * _Nullable error);

@interface PMJSONParser : NSObject

-(void)parse:(NSData * _Nullable)data parserHandler:(ParserHandler)parserHandler;

@end
