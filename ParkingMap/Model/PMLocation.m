//
//  PMLocation.m
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import "PMLocation.h"
#import "PMZone.h"

@interface PMLocation ()

@property (nonatomic) MKCoordinateRegion region;
@property (nonatomic) NSArray *zones;
@property (nonatomic) NSNumber *maxPrice;
@property (nonatomic) NSNumber *minPrice;

@end

@implementation PMLocation

-(instancetype)initWithBounds:(NSDictionary *)dictionary center:(NSString *)center zones:(NSArray *)zones {
    self = [super self];
    
    if (self) {
        NSNumber *north = dictionary[@"north"];
        NSNumber *south = dictionary[@"south"];
        NSNumber *west = dictionary[@"west"];
        NSNumber *east = dictionary[@"east"];
        
        CLLocation *northLocation = [[CLLocation alloc] initWithLatitude:[north doubleValue] longitude:0];
        CLLocation *southLocation = [[CLLocation alloc] initWithLatitude:[south doubleValue] longitude:0];
        CLLocation *westLocation = [[CLLocation alloc] initWithLatitude:0 longitude:[west doubleValue]];
        CLLocation *eastLocation = [[CLLocation alloc] initWithLatitude:0 longitude:[east doubleValue]];
        
        CLLocationDistance lat = [northLocation distanceFromLocation:southLocation];
        CLLocationDistance lon = [westLocation distanceFromLocation:eastLocation];
        
        CLLocationCoordinate2D coord = [self calculateCenter:center];
        _region = MKCoordinateRegionMakeWithDistance(coord, lat, lon);
        
        _zones = [self zonesWithArray:zones];
    }
    
    return self;
}

-(CLLocationCoordinate2D)calculateCenter:(NSString *)string {
    NSArray *points = [string componentsSeparatedByString:@" "];
    NSNumber *la = points[0];
    NSNumber *lo = points[1];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([la doubleValue], [lo doubleValue]);
    return coordinate;
}

-(NSArray *)zonesWithArray:(NSArray *)info {
    NSMutableArray *zones = [[NSMutableArray alloc] initWithCapacity:info.count];
    
    for (NSDictionary *dict in info) {
        PMZone *zone = [[PMZone alloc] initWithDictionary:dict];
        [zones addObject:zone];
        
        if (zone.servicePrice < self.minPrice) {
            self.minPrice = zone.servicePrice;
        } else if (zone.servicePrice > self.maxPrice) {
            self.maxPrice = zone.servicePrice;
        }
    }
    
    return zones;
}

@end
