//
//  PMZone.m
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import "PMZone.h"

@interface PMZone ()

@property (nonatomic) NSString *zoneID;
@property (nonatomic) MKPolygon *polygon;
@property (nonatomic) NSString *name;
@property (nonatomic) BOOL paymentAllowed;
@property (nonatomic) NSNumber *maxDuration;
@property (nonatomic) NSNumber *servicePrice;
@property (nonatomic) NSNumber *depth;
@property (nonatomic) NSNumber *draw;
@property (nonatomic) BOOL stickerRequired;
@property (nonatomic) NSString *currency;
@property (nonatomic) NSString *email;
@property (nonatomic) CLLocationCoordinate2D center;
@property (nonatomic) NSString *country;
@property (nonatomic) NSString *providerID;
@property (nonatomic) NSString *providerName;

@end

@implementation PMZone

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super self];
    
    if (self) {
        _zoneID = dictionary[@"id"];
        _polygon = [self calculatePolygon:dictionary[@"polygon"]];
        _name = dictionary[@"name"];
        _paymentAllowed = [dictionary[@"payment_is_allowed"] boolValue];
        _maxDuration = dictionary[@"max_duration"];
        _servicePrice = dictionary[@"service_price"];
        _depth = dictionary[@"depth"];
        _draw = dictionary[@"draw"];
        _stickerRequired = [dictionary[@"sticker_required"] boolValue];
        _currency = dictionary[@"currency"];
        _email = dictionary[@"contact_email"];
        _center = [self calculateCenter:dictionary[@"point"]];
        _country = dictionary[@"country"];
        _providerID = dictionary[@"provider_id"];
        _providerName = dictionary[@"provider_name"];
    }
    
    return self;
}

-(MKPolygon *)calculatePolygon:(NSString *)string {
    NSArray *pairs = [string componentsSeparatedByString:@", "];
    
    CLLocationCoordinate2D coords[pairs.count];
    
    for (NSInteger i = 0; i < pairs.count; i++) {
        NSString *pair = pairs[i];
        NSArray *points = [pair componentsSeparatedByString:@" "];
        NSNumber *la = points[0];
        NSNumber *lo = points[1];
        coords[i] = CLLocationCoordinate2DMake([la doubleValue], [lo doubleValue]);
    }
    
    MKPolygon *polygon = [MKPolygon polygonWithCoordinates:coords count:pairs.count];
    [polygon setTitle:_name];
    
    return polygon;
}

-(CLLocationCoordinate2D)calculateCenter:(NSString *)string {
    NSArray *points = [string componentsSeparatedByString:@" "];
    NSNumber *la = points[0];
    NSNumber *lo = points[1];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([la doubleValue], [lo doubleValue]);
    return coordinate;
}

-(NSString *)formattedPrice {
    return [NSString stringWithFormat:@"Price: %@%.2f", self.currency, [self.servicePrice doubleValue]];
}

@end
