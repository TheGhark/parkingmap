//
//  PMDetailsViewController.m
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import "PMDetailsViewController.h"

@interface PMDetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentAllowedLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *stickerRequired;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *providerLabel;

@end

@implementation PMDetailsViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Details";
    [self fill];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)fill {
    [self.nameLabel setText:self.zone.name];
    [self.paymentAllowedLabel setText:self.zone.paymentAllowed ? @"Yes" : @"No"];
    
    NSInteger minutes = [self.zone.maxDuration doubleValue]/12;
    [self.durationLabel setText:[NSString stringWithFormat:@"%ld minutes", minutes]];
    
    [self.priceLabel setText:self.zone.formattedPrice];
    [self.stickerRequired setText:self.zone.stickerRequired ? @"Yes": @"No"];
    [self.emailLabel setText:self.zone.email];
    [self.countryLabel setText:self.zone.country];
    [self.providerLabel setText:self.zone.providerName];
}

@end
