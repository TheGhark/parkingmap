//
//  PMJSONParser.m
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import "PMJSONParser.h"

@implementation PMJSONParser

-(void)parse:(NSData *)data parserHandler:(ParserHandler)parserHandler {
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        parserHandler(nil, error);
    } else {
        NSString *center = json[@"current_location"];
        NSDictionary *info = json[@"location_data"];
        NSDictionary *bounds = info[@"bounds"];
        NSArray *zones = info[@"zones"];
        
        PMLocation *location = [[PMLocation alloc] initWithBounds:bounds center:center zones:zones];
        parserHandler(location, nil);
    }
}

@end
