//
//  AppDelegate.h
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 11/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

