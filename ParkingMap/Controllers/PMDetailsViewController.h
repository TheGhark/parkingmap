//
//  PMDetailsViewController.h
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMZone.h"

@interface PMDetailsViewController : UITableViewController

@property (nonatomic) PMZone *zone;

@end
