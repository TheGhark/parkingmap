//
//  MKPolygon+Utils.h
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MKPolygon (Utils)

-(BOOL)containsCoordinate:(CLLocationCoordinate2D)coordinate;
-(BOOL)containsMapPoint:(MKMapPoint)mapPoint;

@end
