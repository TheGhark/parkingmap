//
//  PMPinAnnotation.h
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 11/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PMPinAnnotation : NSObject <MKAnnotation>

@property (nonatomic) NSString *pinTitle;
@property (nonatomic) NSString *pinPrice;

-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
