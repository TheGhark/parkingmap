//
//  PMPinAnnotation.m
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 11/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import "PMPinAnnotation.h"

@implementation PMPinAnnotation
@synthesize coordinate;

-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)coord {
    self = [super self];
     
    if (self) {
        self.coordinate = coord;
    }
    
    return self;
}

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    coordinate = newCoordinate;
}

-(NSString *)title {
    return self.pinTitle;
}

-(NSString *)subtitle {
    return self.pinPrice;
}

@end
