//
//  MKPolygon+Utils.m
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import "MKPolygon+Utils.h"

@implementation MKPolygon (Utils)

-(BOOL)containsCoordinate:(CLLocationCoordinate2D)coordinate {
    MKMapPoint mapPoint = MKMapPointForCoordinate(coordinate);
    return [self containsMapPoint:mapPoint];
}

-(BOOL)containsMapPoint:(MKMapPoint)mapPoint {
    MKPolygonRenderer *polygonRenderer = [[MKPolygonRenderer alloc] initWithPolygon:self];
    CGPoint polygonViewPoint = [polygonRenderer pointForMapPoint:mapPoint];
    return CGPathContainsPoint(polygonRenderer.path, NULL, polygonViewPoint, YES);
}

@end
