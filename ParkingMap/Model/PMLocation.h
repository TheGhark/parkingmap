//
//  PMLocation.h
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PMLocation : NSObject

@property (readonly, nonatomic) MKCoordinateRegion region;
@property (readonly, nonatomic) NSArray *zones;
@property (readonly, nonatomic) NSNumber *maxPrice;
@property (readonly, nonatomic) NSNumber *minPrice;

-(instancetype)initWithBounds:(NSDictionary *)dictionary center:(NSString *)center zones:(NSArray *)zones;

@end
