//
//  PMMapViewController.m
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 11/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import "PMMapViewController.h"
#import "PMPinAnnotation.h"
#import "PMJSONParser.h"
#import "PMLocation.h"
#import "PMZone.h"
#import "MKPolygon+Utils.h"
#import "PMDetailsViewController.h"

NSString *kAnnotationIdentifier = @"AnnotationIdentifier";

@interface PMMapViewController ()

@property (weak, nonatomic) IBOutlet UIButton *parkButton;

@property (nonatomic) PMPinAnnotation *pinAnnotation;
@property (nonatomic) PMJSONParser *parser;
@property (nonatomic) PMLocation *location;
@property (nonatomic) PMZone *selectedZone;
@property (nonatomic) BOOL initialized;

@end

@implementation PMMapViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.delegate = self;
    self.initialized = NO;
    
    [self loadJSON];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self addLocation];
}

#pragma - Setters

#pragma - Actions

-(IBAction)parkButtonTapped:(id)sender {
    if (self.selectedZone) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"Parked"
                                    message:[NSString stringWithFormat:@"Currently parking at %@", self.selectedZone.name]
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma - Private

-(void)showParkingLotDetails {
    [self performSegueWithIdentifier:@"showDetails" sender:nil];
}

-(void)loadJSON {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    _parser = [PMJSONParser new];
    
    [_parser parse:data parserHandler:^(PMLocation * _Nullable location, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error while parsing JSON:%@", error);
        } else {
            self.location = location;
        }
    }];
}

-(void)addLocation {
    if (!self.initialized) {
        self.initialized = YES;
        
        [self.mapView setRegion:self.location.region animated:YES];
        
        for (PMZone *zone in self.location.zones) {
            [self.mapView addOverlay:zone.polygon];
        }
    }
}

-(PMZone *)zoneAtCoordinate:(CLLocationCoordinate2D)coord {
    for (PMZone *zone in self.location.zones) {
        if ([zone.polygon containsCoordinate:coord]) {
            return zone;
        }
    }
    
    return nil;
}

-(PMZone *)zoneForPolygon:(MKPolygon *)polygon {
    for (PMZone *zone in self.location.zones) {
        if ([zone.polygon isEqual:polygon]) {
            return zone;
        }
    }
    
    return nil;
}

-(UIColor *)colorForPolygon:(MKPolygon *)polygon {
    PMZone *zone = [self zoneForPolygon:polygon];
    CGFloat alpha = MIN([zone.servicePrice doubleValue], 1);
    return [[UIColor orangeColor] colorWithAlphaComponent:alpha];
}

#pragma - Overridden

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ((PMDetailsViewController *)[segue destinationViewController]).zone = self.selectedZone;
}

#pragma - MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    self.pinAnnotation = [[PMPinAnnotation alloc] initWithCoordinate:mapView.centerCoordinate];
    [mapView addAnnotation:self.pinAnnotation];
    
    NSArray *overlays = mapView.overlays;
    MKPolygon *selectedPolygon;
    
    for (id<MKOverlay>overlay in overlays) {
        MKPolygon *polygon = (MKPolygon *)overlay;
        MKPolygonRenderer *renderer = (MKPolygonRenderer *)[mapView rendererForOverlay:overlay];
        
        if ([polygon containsCoordinate:mapView.centerCoordinate]) {
            renderer.fillColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
            selectedPolygon = polygon;
        } else {
            renderer.fillColor = [self colorForPolygon:polygon];
        }
    }
    
    self.selectedZone = [self zoneForPolygon:selectedPolygon];
    
    BOOL hidden = self.selectedZone == nil;
    [self.parkButton setHidden:hidden];
}

-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    [mapView removeAnnotation:self.pinAnnotation];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isKindOfClass:[PMPinAnnotation class]]) {
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:kAnnotationIdentifier];
        
        if (!pinView) {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:kAnnotationIdentifier];
            pinView.pinTintColor = [UIColor redColor];
            pinView.animatesDrop = YES;
            pinView.canShowCallout = YES;
        } else {
            pinView.annotation = annotation;
        }
        
        PMPinAnnotation *pin = (PMPinAnnotation *)annotation;
        PMZone *zone = [self zoneAtCoordinate:pin.coordinate];
        
        if (zone) {
            pin.pinPrice = zone.formattedPrice;
            pin.pinTitle = zone.name;
        }
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:nil action:@selector(showParkingLotDetails) forControlEvents:UIControlEventTouchUpInside];
        pinView.rightCalloutAccessoryView = rightButton;
        
        return pinView;
    }
    
    return nil;
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    if ([overlay isKindOfClass:[MKPolygon class]]) {
        MKPolygonRenderer *aRenderer = [[MKPolygonRenderer alloc] initWithPolygon:(MKPolygon*)overlay];
        aRenderer.fillColor = [self colorForPolygon:(MKPolygon *)overlay];
        aRenderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.75];
        aRenderer.lineWidth = 2;
        
        return aRenderer;
    }
    
    return nil;
}

@end
