//
//  PMZone.h
//  ParkingMap
//
//  Created by Camilo Rodriguez Gaviria on 12/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PMZone : NSObject

@property (readonly, nonatomic) NSString *zoneID;
@property (readonly, nonatomic) MKPolygon *polygon;
@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) BOOL paymentAllowed;
@property (readonly, nonatomic) NSNumber *maxDuration;
@property (readonly, nonatomic) NSNumber *servicePrice;
@property (readonly, nonatomic) NSNumber *depth;
@property (readonly, nonatomic) NSNumber *draw;
@property (readonly, nonatomic) BOOL stickerRequired;
@property (readonly, nonatomic) NSString *currency;
@property (readonly, nonatomic) NSString *email;
@property (readonly, nonatomic) CLLocationCoordinate2D center;
@property (readonly, nonatomic) NSString *country;
@property (readonly, nonatomic) NSString *providerID;
@property (readonly, nonatomic) NSString *providerName;
@property (nonatomic) NSString *formattedPrice;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
